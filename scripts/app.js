'use strict';

// declare modules
angular.module('Authentication', []);
angular.module('Home', []);
angular.module('Users', []);
angular.module('Cameras', []);
angular.module('Reports', []);

angular.module('Spotted', [
    'Authentication',
    'Home',
    'Users',
    'Cameras',
    'Reports',
    'ngRoute',
    'ngCookies',
    'ngIdle'
])

    .config(['$routeProvider', 'IdleProvider', 'KeepaliveProvider', function ($routeProvider, IdleProvider, KeepaliveProvider) {
        $routeProvider
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'modules/authentication/views/login.html'
            })
            .when('/logout', {
                redirectTo: 'login'
            })
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'views/template.html'
            })
            .when('/users', {
                controller: 'UsersController',
                templateUrl: 'views/template.html'
            })
            .when('/users/create', {
                controller: 'CreateUserController',
                templateUrl: 'views/template.html'
            })
            .when('/users/:user_id', {
                controller: 'UserController',
                templateUrl: 'views/template.html'
            })
            .when('/cameras', {
                controller: 'CameraController',
                templateUrl: 'views/template.html'
            })
            .when('/cameras/create', {
                controller: 'CreateCameraController',
                templateUrl: 'views/template.html'
            })
            .when('/camera/:user_id', {
                controller: 'CameraProfileController',
                templateUrl: 'views/template.html'
            })
            .when('/reports', {
                controller: 'CameraController',
                templateUrl: 'views/template.html'
            })
            .when('/404', {
                controller: 'ReportController',
                templateUrl: 'views/template.html'
            })
            .otherwise({redirectTo: '/'});

        IdleProvider.idle(900); // 15 min
        IdleProvider.timeout(600);
        KeepaliveProvider.interval(500); // heartbeat every 10 min
        KeepaliveProvider.http('/api/heartbeat'); // URL that makes sure session is alive
    }])

    .run(['$rootScope', '$location', '$cookieStore', '$http', 'Idle',
        function ($rootScope, $location, $cookieStore, $http, Idle) {
            // keep user logged in after page refresh
            $rootScope.globals = $cookieStore.get('globals') || {};

            if ($rootScope.globals.currentUser) {
                $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line

                Idle.watch();
                $rootScope.$on('IdleStart', function () {
                    alert("Your session is about to timeout. Please click anywhere to avoid being logged out")
                });
                $rootScope.$on('IdleTimeout', function () { /* Logout user */
                    $location.path('/logout');
                });
            }
            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                // redirect to login page if not logged in
                if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                    $location.path('/login');
                }
            });
        }]);