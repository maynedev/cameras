'use strict';

angular.module('Users')

    .controller('UsersController', ['$http', '$scope', '$rootScope', 'RegisterService',
        function ($http, $scope, $rootScope, RegisterService) {
            $scope.body = "modules/users/views/users.html";
            $scope.page = {title: "Registered users", url: "/users", active: "users"};
            $scope.start = 1;
            $scope.users = [
                {
                    "UserName": "amiro",
                    "ProviderUserKey": "de97d390-530e-435a-9847-735978329f68",
                    "Email": "razhick@spottedanywhere.com",
                    "PasswordQuestion": "What is your favorite color?",
                    "Comment": "",
                    "IsApproved": true,
                    "IsLockedOut": false,
                    "LastLockoutDate": "2017-06-05T14:31:33.443+03:00",
                    "CreationDate": "2017-05-29T10:06:13.94+03:00",
                    "LastLoginDate": "2017-06-05T14:40:22.107+03:00",
                    "LastActivityDate": "2017-06-05T14:40:22.097+03:00",
                    "LastPasswordChangedDate": "2017-06-05T14:32:07.82+03:00",
                    "IsOnline": false,
                    "ProviderName": "CloudMembershipProvider"
                },
                {
                    "UserName": "safcom",
                    "ProviderUserKey": "0d52016",
                    "Email": "joenjuguna53@gmail.com",
                    "PasswordQuestion": "What is your favorite color?",
                    "Comment": "",
                    "IsApproved": true,
                    "IsLockedOut": false,
                    "LastLockoutDate": "2017-06-05T11:38:58.5+03:00",
                    "CreationDate": "2017-05-29T10:02:52.92+03:00",
                    "LastLoginDate": "2017-06-05T14:48:14.733+03:00",
                    "LastActivityDate": "2017-06-05T14:48:14.72+03:00",
                    "LastPasswordChangedDate": "2017-06-05T11:39:34.323+03:00",
                    "IsOnline": false,
                    "ProviderName": "CloudMembershipProvider"
                }
            ];


            // $http.get("http://test.url").then(function (response) {
            //     $scope.users = response.data;
            // });

        }])
    .controller('UserController', ['$http', '$routeParams', '$scope', '$rootScope', 'RegisterService',
        function ($http, $routeParams, $scope, $rootScope, RegisterService) {

            console.log($routeParams.user_id);
            $scope.body = "modules/users/views/profile.html";
            $scope.page = {title: "User profile", url: "/users", active: "users"};
            $scope.cameras = [
                {
                    "UserName": "amiro",
                    "CameraName": "001",
                    "City": "City under The Sun",
                    "Description": "What is your favorite color?",
                    "Latitude": "",
                    "Longitude": "",
                    "AllocatedSpace": 100,
                    "DirectorySize": 30,
                    "LastestFileCreationTime": "2017-06-05T14:31:33.443+03:00"
                },
                {
                    "UserName": "amiro",
                    "CameraName": "002",
                    "City": "City over The Moon",
                    "Description": "Now throw your hands in the sky",
                    "Latitude": "",
                    "Longitude": "",
                    "AllocatedSpace": 100,
                    "DirectorySize": 30,
                    "LastestFileCreationTime": "2017-06-05T14:31:33.443+03:00"
                }
            ];
            $scope.user =
                {
                    "UserName": "amiro",
                    "ProviderUserKey": "de97d390-530e-435a-9847-735978329f68",
                    "Email": "razhick@spottedanywhere.com",
                    "Phone": "072345878736",
                    "PasswordQuestion": "What is your favorite color?",
                    "Comment": "",
                    "IsApproved": true,
                    "IsLockedOut": false,
                    "LastLockoutDate": "2017-06-05T14:31:33.443+03:00",
                    "CreationDate": "2017-05-29T10:06:13.94+03:00",
                    "LastLoginDate": "2017-06-05T14:40:22.107+03:00",
                    "LastActivityDate": "2017-06-05T14:40:22.097+03:00",
                    "LastPasswordChangedDate": "2017-06-05T14:32:07.82+03:00",
                    "IsOnline": false,
                    "ProviderName": "CloudMembershipProvider"
                };

            $scope.username = $scope.user.UserName;
            $scope.email = $scope.user.Email;
            $scope.phone = $scope.user.Phone;
            $scope.IsValid = $scope.user.IsApproved;
            $scope.comment = "admin";// $scope.user.Comment;

            /*
             Uncomment this to load real data
             */

            // $http.get("http://test.url").then(function (response) {
            //     $scope.user = response.data;
            // });
            $scope.delete = function () {
                RegisterService.Delete($scope.username, function (response) {
                    $scope.dataLoading = true;
                    if (response.code === 200) {
                        $scope.success = "User deleted successfully";
                        $location.path('/users');
                    } else {
                        $scope.error = "Error! User could not be deleted. ";
                        $scope.dataLoading = false;
                    }
                });
            };

            $scope.update = function () {
                $scope.dataUpdating = true;
                RegisterService.Update($scope.username, $scope.email, $scope.phone, $scope.password, $scope.comment,
                    function (response) {
                        if (response.code === 200) {
                            $scope.success = "User updated successfully";
                        } else {
                            $scope.error = "Error! User details could not be updated. ";
                            $scope.dataUpdating = false;
                        }
                    });
            }
        }])


    .controller('CreateUserController',
        ['$scope', '$rootScope', 'RegisterService',
            function ($scope, $rootScope, RegisterService) {
                $scope.body = "modules/users/views/create.html";
                $scope.page = {title: "Register users", url: "/users/create", active: "users"};

                $scope.register = function () {
                    $scope.dataLoading = true;
                    RegisterService.Register($scope.username, $scope.email, $scope.phone, $scope.password, $scope.isValid, $scope.comment,

                        function (response) {
                            console.log(response);
                            if (response.success) {
                                $scope.error = response.message;
                                $location.path('/');
                            } else {
                                $scope.error = response.message;
                                $scope.dataLoading = false;
                            }
                        });
                };
            }]);