'use strict';

angular.module('Users')

    .factory('RegisterService', ['$http',
        function ($http) {
            var service = {};

            service.Register = function (username, email, phone, password, isValid, comment, callback) {
                $http.post('http://172.18.234.75/data/cloudcreateuser.ashx',
                    {
                        username: username,
                        password: password,
                        email: email,
                        phone: phone,
                        isValid: isValid,
                        comment: comment
                    })
                    .success(function (response) {
                        callback(response);
                    });
            };

            service.Delete = function (username, callback) {
                $http.post('/api/authenticate',
                    {username: username})
                    .success(function (response) {
                        callback(response);
                    });

            };
            service.Update = function (username, email, phone, password, isValid, comment, callback) {

                $http.post('/api/authenticate',
                    {
                        username: username,
                        password: password,
                        email: email,
                        isValid: isValid,
                        phone: phone,
                        comment: comment
                    })
                    .success(function (response) {
                        callback(response);
                    });
            };
            return service;
        }]);
