'use strict';

angular.module('Home')

    .controller('HomeController',
        ['$scope',
            function ($scope) {
                $scope.body = "modules/home/views/home.html";
                $scope.page = {title: "Dashboard", url: "/", active: "home"};
            }]);