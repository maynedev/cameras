'use strict';

angular.module('Cameras')

    .controller('CameraController', ['$scope', '$http', function ($scope, $http) {
        $scope.body = "modules/cameras/views/cameras.html";
        $scope.cameras = [
            {
                "UserName": "amiro",
                "CameraName": "001",
                "City": "City under The Sun",
                "Description": "What is your favorite color?",
                "Latitude": "",
                "Longitude": "",
                "AllocatedSpace": 100,
                "DirectorySize": 30,
                "LastestFileCreationTime": "2017-06-05T14:31:33.443+03:00"
            },
            {
                "UserName": "amiro",
                "CameraName": "002",
                "City": "City over The Moon",
                "Description": "Now throw your hands in the sky",
                "Latitude": "",
                "Longitude": "",
                "AllocatedSpace": 100,
                "DirectorySize": 30,
                "LastestFileCreationTime": "2017-06-05T14:31:33.443+03:00"
            }
        ];

        // $http.post('path/to/api', //todo:replace with real api
        //     {})
        //     .success(function (response) {
        //         if (response === '200') {
        //             return response;
        //         } else {
        //             $scope.error = response.message;
        //         }
        //     });
    }])

    .controller('CreateCameraController', ['$scope', '$http', function ($scope, $http) {
        $scope.body = "modules/cameras/views/create.html";

        $scope.register = function () {
            $scope.dataLoading = true;

            $http.post('path/to/api', //todo:replace with real api
                {
                    username: $scope.username,
                    cameraname: $scope.camera_no,
                    allocatedspace: $scope.space,
                    city: $scope.city,
                    package: $scope.package,
                    description: $scope.description
                })
                .success(function (response) {
                    if (response === '200') {
                        $location.path('/');
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });
        };
    }])
    .controller('CameraProfileController', ['$scope', '$http', function ($scope, $http) {
        $scope.body = "modules/cameras/views/profile.html";
        $scope.camera = {
            "UserName": "amiro",
            "CameraName": "001",
            "City": "City under The Sun",
            "Description": "What is your favorite color?",
            "Latitude": "",
            "Longitude": "",
            "AllocatedSpace": 100,
            "DirectorySize": 30,
            "LastestFileCreationTime": "2017-06-05T14:31:33.443+03:00"
        };
        $scope.logs = [
            {
                "UserName": "amiro",
                "CameraName": "001",
                "City": "City under The Sun",
                "Description": "What is your favorite color?",
                "Latitude": "",
                "Longitude": "",
                "AllocatedSpace": 100,
                "DirectorySize": 30,
                "LastestFileCreationTime": "2017-06-05T14:31:33.443+03:00"
            },
            {
                "UserName": "amiro",
                "CameraName": "002",
                "City": "City over The Moon",
                "Description": "Now throw your hands in the sky",
                "Latitude": "",
                "Longitude": "",
                "AllocatedSpace": 100,
                "DirectorySize": 30,
                "LastestFileCreationTime": "2017-06-05T14:31:33.443+03:00"
            }
        ];


        $scope.delete = function () {

            $scope.dataUpdating = true;
            $http.post('path/to/api', //todo:replace with real api
                {
                    username: $scope.username,
                    cameraname: $scope.camera_no,
                    allocatedspace: $scope.space
                })
                .success(function (response) {
                    if (response === '200') {
                        $location.path('/');
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });
        };

        $scope.update = function () {
            $scope.dataUpdating = true;
            $http.post('path/to/api', //todo:replace with real api
                {
                    username: $scope.username,
                    cameraname: $scope.camera_no,
                    allocatedspace: $scope.space,
                    city: $scope.city,
                    package: $scope.package,
                    description: $scope.description
                })
                .success(function (response) {
                    if (response === '200') {
                        $location.path('/');
                    } else {
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });

        }
    }]);