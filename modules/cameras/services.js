'use strict';

angular.module('Cameras')

    .factory('CrudService', ['$http', '$timeout',
        function ($http, $timeout) {
            var service = {};

            service.Register = function (values,endpoint, callback) {

                /* Dummy authentication for testing, uses $timeout to simulate api call
                 ----------------------------------------------*/

                console.log(values);

                /* Use this for real authentication
                 ----------------------------------------------*/
                $http.post(endpoint,
                    {username: username,cameraname: c_no,allocatedspace: space, city: city,package: c_package, description: description})
                    .success(function (response) {
                        callback(response);
                    });

            };

            service.Delete = function (username, callback) {

                /* Dummy authentication for testing, uses $timeout to simulate api call
                 ----------------------------------------------*/
                $timeout(function () {
                    var response = {success: username === 'test' && password === 'test'};
                    if (!response.success) {
                        response.message = 'Username or password is incorrect';
                    }
                    callback(response);
                }, 1000);

                /* Use this for real authentication
                 ----------------------------------------------*/
                // $http.post('/api/authenticate',
                //     {username: username})
                //     .success(function (response) {
                //         callback(response);
                //     });

            };
            service.Update = function (username, email, phone, password, comment, callback) {

                /* Dummy authentication for testing, uses $timeout to simulate api call
                 ----------------------------------------------*/
                $timeout(function () {
                    var response = {success: username === 'test' && password === 'test'};
                    if (!response.success) {
                        response.message = 'Username or password is incorrect';
                    }
                    callback(response);
                }, 1000);

                /* Use this for real authentication
                 ----------------------------------------------*/
                // $http.post('/api/authenticate',
                //     {username: username, password: password, email: email, phone: phone, comment: comment})
                //     .success(function (response) {
                //         callback(response);
                //     });

            };
            return service;
        }]);
